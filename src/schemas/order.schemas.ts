import {array, date, number, object, string, TypeOf} from "zod";
import {str} from "envalid";


export const createOrderSchema = object({
    body: object({
        customer_name: string({required_error: "Customer name is required!"}),
        comments: string(),
        order_address: string({required_error: "Address is required!"}),
        order_details: array(object({
            product_id: string({required_error: "Product_id is must provided!"}),
            quantity: number({required_error: "quantity is required", invalid_type_error: "quantity must be a number"}),
            total: number({required_error: "total is required!"})
        }))
    })
})

export type CreateOrderInput = TypeOf<typeof createOrderSchema>['body']