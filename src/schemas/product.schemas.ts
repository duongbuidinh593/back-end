import {array, number, object, string, TypeOf} from "zod";


export const createProductSchema = object({
    body:object({
        name: string({
            required_error:"Product name is required!"
        }).min(2),
        price:number({
            invalid_type_error:"Price must be a number!",
            required_error:"Price is required!"
        }).min(1),
        type:string({required_error:"Type's product is required!"}),
        description:string({required_error:"Description is required!"}),
        discount:number({
            invalid_type_error:"Discount must be a number!",
        }),
    })
})

export type CreateProductInput = TypeOf<typeof createProductSchema>['body']