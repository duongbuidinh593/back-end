import {AppDataSource} from "../utils/data-source";
import {Cart} from "../entities/cart.entity";
import {CreateCartInput} from "../schemas/cart.schemas";
import {getProductWithID} from "./product.service";
import {findUserById} from "./user.service";

const cartRepository = AppDataSource.getRepository(Cart)


export const createCart = async (input: CreateCartInput, userId: string) => {

    const product = await getProductWithID(input.product_id);
    const user = await findUserById(userId);

    if (product !== null && user !== null) {
        return await cartRepository.save(cartRepository.create({product, user, quantity: input.quantity}));
    }

    throw new Error("Product does not exist!")
}

export const getAllCartWithUserId = async (userId: string) => {
    return await cartRepository.createQueryBuilder('carts').leftJoinAndSelect('carts.product', 'product').where('carts.user_id = :id', {id: userId}).getMany()
}

export const removeAllCartWithUserId = async (userId: string) => {
    const carts = await getAllCartWithUserId(userId)
    return await cartRepository.remove(carts)
}

export const updateCartById = async (cartId: string, quantity: number) => {
    const cart = await cartRepository.findOneBy({id: cartId});
    if (cart !== null) {
        cart.quantity = quantity;
        return await cartRepository.save(cart);
    }

    throw new Error(`Cart with #${cartId} was not found!`)

}

export const removeCartById = async (cartId: string) => {
    return await cartRepository.delete({id: cartId});
}