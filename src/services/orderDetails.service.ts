import {OrderDetails} from "../entities/orderDetails.entity";
import {AppDataSource} from "../utils/data-source";
import {getProductWithID} from "./product.service";

const orderDetailRepository = AppDataSource.getRepository(OrderDetails)

export const createOrderDetails = async (input: OrderDetails) => {
    return await orderDetailRepository.save(orderDetailRepository.create(input));
}