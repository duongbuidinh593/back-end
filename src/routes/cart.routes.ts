import express from "express";
import {deserializeUser} from "../middleware/deserializeUser";
import {requireUser} from "../middleware/requireUser";
import {
    createCartHandle,
    getCartByUserIDHandle,
    removeCartWithId,
    removeCartWithUserIDHandle, updateQuantityCartItem
} from "../controllers/cart.controller";


const cartRouter = express.Router()

cartRouter.post('/create_cart', deserializeUser, requireUser, createCartHandle);

cartRouter.get('/get_cart', deserializeUser, requireUser, getCartByUserIDHandle)

cartRouter.get('/remove_all_cart', deserializeUser, requireUser, removeCartWithUserIDHandle)

cartRouter.get('/remove_cart/:cartID', deserializeUser,requireUser, removeCartWithId)

cartRouter.post('/update_cart', deserializeUser, requireUser, updateQuantityCartItem)
export default cartRouter;