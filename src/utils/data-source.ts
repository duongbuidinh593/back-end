require('dotenv').config();
import 'reflect-metadata';
import {DataSource} from 'typeorm';
import config from 'config';

const postgresConfig = config.get<{
    host: string;
    port: number;
    username: string;
    password: string;
    database: string;
}>('postgresConfig');

export const AppDataSource = new DataSource({
    //  ...postgresConfig,
    host: "dpg-cci8g05a4995109k7a8g-a.singapore-postgres.render.com",
    port: 5432,
    username: "admin",
    password: "xjFCCLHXpmsn8ea3Nb7lacc98EkC8uQ3",
    database: "shop_jex3",
    ssl:true,
    type: 'postgres',
    synchronize: true,

    logging: false,
    entities: ['src/entities/**/*.entity{.ts,.js}'],
    migrations: ['src/migrations/**/*{.ts,.js}'],
    subscribers: ['src/subscribers/**/*{.ts,.js}'],
});