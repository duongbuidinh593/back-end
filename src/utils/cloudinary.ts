import {v2 as cloudinary} from "cloudinary"
import config from "config";

const cloudinaryConfig = config.get<{
    cloudinaryName: string,
    cloudinaryApiKey: string,
    cloudinaryApiSecretKey: string,
}>('cloudinary')

cloudinary.config({
    cloud_name: cloudinaryConfig.cloudinaryName,
    api_key: cloudinaryConfig.cloudinaryApiKey,
    api_secret: cloudinaryConfig.cloudinaryApiSecretKey
})

export const uploadImage = async (filepath: string) => {
    const result = await cloudinary.uploader.upload(filepath, {
        folder: "Shop", transformation: {
            height: 629, width:
                500
        }
    });
    return result;
}

export const deleteImage = async (publicIds: string[]) => {
    return await cloudinary.api.delete_resources(publicIds)
}