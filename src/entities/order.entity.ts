import {Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import {User} from "./user.entity";
import {OrderDetails} from "./orderDetails.entity";

@Entity('orders')
export class Order {
    @PrimaryGeneratedColumn('increment',{name:"order_number"})
    orderNumber: number


    @CreateDateColumn({name: "order_date"})
    orderDate: Date

    @Column({type: 'text', name: "status", nullable: false})
    status: string

    @Column({type: 'text', name: 'customer_name', nullable: false})
    fullName: string

    @Column({type: 'text', name: 'comments', nullable: false})
    comments: string

    @Column({type: 'text', name: "order_address", nullable: false})
    orderAddress: string

    @OneToMany(()=>OrderDetails,orderDetails => orderDetails.order)
    orderDetails: OrderDetails[]

    @ManyToOne(() => User)
    @JoinColumn({name:"user_id"})
    user: User

}