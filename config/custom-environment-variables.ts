export default {
    port: 'PORT',
    postgresConfig: {
        host: 'POSTGRES_HOST',
        port: 'POSTGRES_PORT',
        username: 'POSTGRES_USER',
        password: 'POSTGRES_PASSWORD',
        database: 'POSTGRES_DB',
    },
    accessTokenPrivateKey: 'JWT_ACCESS_TOKEN_PRIVATE_KEY',
    accessTokenPublicKey: 'JWT_ACCESS_TOKEN_PUBLIC_KEY',
    refreshTokenPrivateKey: 'JWT_REFRESH_TOKEN_PRIVATE_KEY',
    refreshTokenPublicKey: 'JWT_REFRESH_TOKEN_PUBLIC_KEY',
    smtp: {
        host: 'EMAIL_HOST',
        pass: 'EMAIL_PASS',
        port: 'EMAIL_PORT',
        user: 'EMAIL_USER',
    },
    googleMailer: {
        clientIDMailer: "GOOGLE_MAILER_CLIENT_ID",
        clientSecretMailer: "GOOGLE_MAILER_CLIENT_SECRET",
        refreshTokenMailer: "GOOGLE_MAILER_REFRESH_TOKEN",
        emailAdminAddress: "ADMIN_EMAIL_ADDRESS"
    },
    cloudinary: {
        cloudinaryName: "CLOUDINARY_CLOUD_NAME",
        cloudinaryApiKey: "CLOUDINARY_API_KEY",
        cloudinaryApiSecretKey: "CLOUDINARY_API_SECRET",
      //  cloudinaryApiEnvironmentVar: "CLOUDINARY_API_ENVIRONMENT_VARIABLE"
    }
};

